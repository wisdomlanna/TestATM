package com.example.potik.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

/**
 * Created by potik on 01-Dec-16.
 */

public class ResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result);
        TextView textView = (TextView) findViewById(R.id.result_balanceAmount);
//        textView.setText(new DecimalFormat("##.##").format(getNetBalance()) + " Baht");
        textView.setText(getNetBalance() + " Baht");
    }

    public void doWithdraw(View view) {
        Intent intent = new Intent(this, BalanceActivity.class);
        intent.putExtra("balanceValue", getNetBalance());
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
//        Intent intent = new Intent(this, BalanceActivity.class);
//        intent.putExtra("balanceValue", getNetBalance());
//        startActivity(intent);

        //prevent user press back button to previous activity
    }

    public Double getNetBalance() {
        Bundle bundle = getIntent().getExtras();
        Double netBalance = bundle.getDouble("netBalance");
        return Double.valueOf(new DecimalFormat("##.##").format(netBalance));
    }

    public void quitApp(View view) {
        finishAffinity();
    }
}

