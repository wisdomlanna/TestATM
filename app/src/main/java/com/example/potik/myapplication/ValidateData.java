package com.example.potik.myapplication;

/**
 * Created by potik on 07-Dec-16.
 */

public class ValidateData {
    public Boolean checkIfNegative(double amount) {
        return (amount > 0) ? true : false;
    }

    public Boolean checkBalance(double deposited, double withdraw) {
        return (deposited - withdraw >= 0) ? true : false;
    }
}
