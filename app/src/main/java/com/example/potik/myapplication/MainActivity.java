package com.example.potik.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ValidateData validate = new ValidateData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendBalance(View view) {
        //should be two decimal for valid input
        EditText editText = (EditText) findViewById(R.id.activityMain_balanceValue);
        Double balance = Double.parseDouble(String.valueOf(editText.getText()));
        if (validate.checkIfNegative(balance)){
            try {
                Intent intent = new Intent(this, BalanceActivity.class);
                intent.putExtra("balanceValue", balance);
                startActivity(intent);
            } catch (Exception e) {
//            Toast.makeText(this, "กรุณาระบุยอดเงินให้ถูกต้อง", Toast.LENGTH_SHORT).show();
                Toast.makeText(this, "Invalid input", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Invalid input", Toast.LENGTH_SHORT).show();
        }
    }

}
