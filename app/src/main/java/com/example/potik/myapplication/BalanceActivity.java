package com.example.potik.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

/**
 * Created by potik on 01-Dec-16.
 */

public class BalanceActivity extends AppCompatActivity {
    ValidateData validate = new ValidateData();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.balance);

        TextView textView = (TextView) findViewById(R.id.balance_balanceAmount);
        textView.setText(getNetBalance() + " Baht");
    }

    @Override
    public void onBackPressed() {
        //Do nothing. prevent user press back button to previous activity
    }

    public void doWithdraw(View view) {
        //should be two decimal for valid input
        try {
            Double deposit = getNetBalance();
            EditText editTextWithdraw = (EditText) findViewById(R.id.balance_withdrawAmount);
            String withdraw = String.valueOf(editTextWithdraw.getText());
            Double Dwithdraw = Double.parseDouble(withdraw);

            if (validate.checkBalance(deposit,Dwithdraw) && validate.checkIfNegative(Dwithdraw)) {
                Double netBalance = deposit - Dwithdraw;
                Toast.makeText(BalanceActivity.this,
                        "Deposited = " + deposit + "\nWithdraw = " + Dwithdraw + "\nNet Balance = " + netBalance,
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(this, ResultActivity.class);
                intent.putExtra("netBalance", netBalance);
                startActivity(intent);
            } else {
//                Toast.makeText(BalanceActivity.this, "ยอดเงินคงเหลือไม่พอหรือ\nยอดจำนวนเงินที่ถอนไม่ถูกต้อง", Toast.LENGTH_SHORT).show();
                Toast.makeText(BalanceActivity.this, "Your balance amount is not enough to withdraw or Invalid input", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
//            Toast.makeText(this, "กรุณาระบุยอดเงินให้ถูกต้อง", Toast.LENGTH_SHORT).show();
            Toast.makeText(this, "Invalid input", Toast.LENGTH_SHORT).show();
        }
    }

    public Double getNetBalance() {
        Bundle bundle = getIntent().getExtras();
        Double netBalance = bundle.getDouble("balanceValue");
        return Double.valueOf(new DecimalFormat("##.##").format(netBalance));
    }
}
